<?php

$router->get('/backupmysql', 'GeneracionBackupController@index');
$router->get('/backupmysql/{id}', 'GeneracionBackupController@indexyear');
$router->get('/backupmysql/{id}/month/{idm}', 'GeneracionBackupController@indexmonth');
$router->get('/backupmysqldetail/', 'GeneracionBackupController@detail');
$router->get('/backupmysqldetail/{id}', 'GeneracionBackupController@detailyear');
$router->get('/backupmysqldetail/{id}/month/{idm}', 'GeneracionBackupController@detailmonth');

